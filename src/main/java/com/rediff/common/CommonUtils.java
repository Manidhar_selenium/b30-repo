package com.rediff.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

//import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class CommonUtils {
	public WebDriver driver = null;
	public Properties webElements=null;
	public Properties envData=null;
	public Logger log=null;

	public void loadPropertyFiles() throws IOException{
//New comment in the common file
		log=Logger.getLogger("rootLogger");
		log.debug("Logger configured sucessfully");
		
		InputStream fis=new FileInputStream("/Users/mkanumukkala/eclipse-b30/reg/src/main/resources/Locators.properties");
		webElements=new Properties();
		webElements.load(fis);
		log.debug("Loading Object Locators property file");
		
		InputStream envFile=new FileInputStream("/Users/mkanumukkala/eclipse-b30/reg/src/main/resources/Environments.properties");
		envData=new Properties();
		envData.load(envFile);
		
		log.debug("Loading Environment property file");

	}
	
	public void openBrowser() {
		System.setProperty("webdriver.chrome.driver", "/Users/mkanumukkala/eclipse-b30/reg/src/main/resources/chromedriver");
		driver = new ChromeDriver();
		String url=envData.getProperty("url");
		driver.get(url);
		log.debug("Opening chrome browser and navigating to Rediff mail application");
	}

	
	public void closeBrowser() {
		driver.close();
		log.debug("Closed Browser");
	}

	public void captureScreenshot() throws IOException {
		TakesScreenshot screen = ((TakesScreenshot) driver);
		File screenContent = screen.getScreenshotAs(OutputType.FILE);
		//FileUtils.copyFile(screenContent, new File("/Users/mkanumukkala/eclipse-b30/SeleniumBasics/Screenshot.png"));
	}
	public Object[][] readExcel() throws EncryptedDocumentException, IOException{
		loadPropertyFiles();
		InputStream excelFile=new FileInputStream("/Users/mkanumukkala/eclipse-b30/reg/src/main/resources/TestData.xlsx");
		Workbook wb=WorkbookFactory.create(excelFile);
		Sheet sheet=wb.getSheetAt(0);
		int lastRowNo=sheet.getLastRowNum()+1;
		log.debug(lastRowNo);
		
		Row row=sheet.getRow(0);
		int lastCellNo=row.getLastCellNum();
		log.debug(lastCellNo);
	    
		Object o[][]=new Object[lastRowNo][lastCellNo];
		
		//Rows
		for(int i=0; i<lastRowNo;i++) {
			row=sheet.getRow(i);
			lastCellNo=row.getLastCellNum();
			
			//Cells
			for(int j=0;j<lastCellNo;j++) {
				Cell cell=row.getCell(j);
				String cellValue=cell.getStringCellValue();
				log.debug(cellValue);
				o[i][j]=cellValue;
			}
		}
		return o;
	}
	
	public static void main(String as[]) throws EncryptedDocumentException, IOException {
		CommonUtils c=new CommonUtils();
		c.readExcel();
	}
}
