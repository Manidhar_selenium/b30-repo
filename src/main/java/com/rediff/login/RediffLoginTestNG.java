package com.rediff.login;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.openqa.selenium.By;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.rediff.common.CommonUtils;

public class RediffLoginTestNG extends CommonUtils {

	@BeforeMethod
	public void beforeTest() throws IOException {
		loadPropertyFiles();
		openBrowser();
	}

	@AfterMethod
	public void afterTest() {
		closeBrowser();
	}

	@Test(dataProvider = "data")
	public void loginWithInvalidCredentials(String username, String password) {
		driver.findElement(By.id(webElements.getProperty("userNameInputId"))).sendKeys(username);
		// 4. enter invalid password
		driver.findElement(By.id(webElements.getProperty("passwordInputId"))).sendKeys(password);
		// 5. click on SignIn button
		driver.findElement(By.name(webElements.getProperty("goButtonName"))).click();
		System.out.println("Entered username, password and clicked on Go button");
		// 6. Validate the error message
		String actualErrorMessage = driver.findElement(By.id(webElements.getProperty("loginErrorMessageId"))).getText();
		System.out.println(actualErrorMessage);
	}

	@DataProvider
	public Object[][] data() throws EncryptedDocumentException, IOException {
		
		return readExcel() ;
	}

}
