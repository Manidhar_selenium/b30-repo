package com.rediff.login;
import java.io.IOException;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.rediff.common.CommonUtils;

public class AlertsDemoTestNG extends CommonUtils {

	@BeforeMethod
	public void beforeTest() throws IOException {
		loadPropertyFiles();
		openBrowser();
	}

	@AfterMethod
	public void afterTest() {
		closeBrowser();
	}

	@Test(priority = 1)
	public void verifyPageTitle() {

		String actualPageTitle = driver.getTitle();
		log.debug("Extractig page titile from RediffLogin page");
		String expectedPageTitle = "Rediffmail232323";

		boolean flag = actualPageTitle.equals(expectedPageTitle);

		Assert.assertFalse(flag);
		log.debug("Validating page titile");

		// Assert.assertTrue(actualPageTitle.equals(expectedPageTitle),"actual Tile is
		// not matching with expected title, hence failing the test case");
		// Assert.assertEquals(actualPageTitle, expectedPageTitle);

		/*
		 * int actualNo=100; int expectedNo=101; Assert.assertEquals(actualNo,
		 * expectedNo);
		 */
	}

	@Test(priority = 2, dependsOnMethods = "verifyPageTitle")
	public void loginWihEmptyCredentials() throws InterruptedException, IOException {
		driver.findElement(By.name(webElements.getProperty("goButtonName"))).click();
		log.debug("Clicked on Go button without entering username and password");

		Thread.sleep(1500);

		// WebDriverWait wait = new WebDriverWait(driver, 30);
		// wait.until(ExpectedConditions.alertIsPresent());

		Alert alert = driver.switchTo().alert();
		log.debug("Switching to alert ");

		String actualAlertText = alert.getText();
		String expectedAlertText = "Please enter a valid user name";
		/*
		 * if (actualAlertText.equals(expectedAlertText)) {
		 * System.out.println("Alert text matched"); } else { throw new
		 * Error("Alert text is not matched"); }
		 */

		Assert.assertEquals(actualAlertText, expectedAlertText);
		log.debug("Verified the alert text after extracting it");
		alert.accept();

		Thread.sleep(1500);
		captureScreenshot();
	}

}
